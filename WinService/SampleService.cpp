/***************************************************************************\
* Provides a sample service class that derives from the service base class - 
* CServiceBase. The sample service logs the service start and stop 
* information to the Application event log, and shows how to run the main 
* function of the service in a thread pool worker thread.

\***************************************************************************/

#include "SampleService.h"
#include "ThreadPool.h"
#include <fstream>

#pragma comment(lib, "ws2_32.lib")


CSampleService::CSampleService(PWSTR pszServiceName, 
								BOOL fCanStop, 
								BOOL fCanShutdown, 
								BOOL fCanPauseContinue) :
	CServiceBase(pszServiceName, fCanStop, fCanShutdown, fCanPauseContinue),
	m_dwTimeout(10 * 1000)
{
	// Create a manual-reset event that is not signaled at first to indicate 
	// the service is stopping.
	m_hStoppingEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (m_hStoppedEvent == NULL)
	{
		throw GetLastError();
	}

	// Create a manual-reset event that is not signaled at first to indicate 
	// the stopped signal of the service.
	m_hStoppedEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	if (m_hStoppedEvent == NULL)
	{
		throw GetLastError();
	}
}


CSampleService::~CSampleService(void)
{
	if (m_hStoppedEvent)
	{
		CloseHandle(m_hStoppedEvent);
		m_hStoppedEvent = NULL;
	}

	if (m_hStoppingEvent)
	{
		CloseHandle(m_hStoppingEvent);
		m_hStoppingEvent = NULL;
	}
}


//
//   FUNCTION: CSampleService::OnStart(DWORD, LPWSTR *)
//
//   PURPOSE: The function is executed when a Start command is sent to the 
//   service by the SCM or when the operating system starts (for a service 
//   that starts automatically). It specifies actions to take when the 
//   service starts. In this code, OnStart logs a service-start 
//   message to the Application log, and queues the main service function for 
//   execution in a thread pool worker thread.
//
//   PARAMETERS:
//   * dwArgc   - number of command line arguments
//   * lpszArgv - array of command line arguments
//
void CSampleService::OnStart(DWORD dwArgc, LPWSTR *lpszArgv)
{
    WriteErrorLogEntry(L"CSampleService::Start: function entry");

	// Log a service start message to the Application log.
	WriteEventLogEntry(L"CppWindowsService in OnStart", EVENTLOG_INFORMATION_TYPE);

	// Queue the main service function for execution in a worker thread.
	CThreadPool::QueueUserWorkItem(&CSampleService::ServiceWorkerThread, this);

    WriteErrorLogEntry(L"CSampleService::Start: function exit");
}


//
//   FUNCTION: CSampleService::ServiceWorkerThread(void)
//
//   PURPOSE: The method performs the main function of the service. It runs 
//   on a thread pool worker thread.
//
void CSampleService::ServiceWorkerThread(void)
{
    WriteErrorLogEntry(L"CSampleService::ServiceWorkerThread: running");

	// Write time to a log file
	std::ofstream fout("C:\\Temp\\Now.txt", std::ios::app);

	// Periodically check if the service is stopping.
	while (WaitForSingleObject(m_hStoppingEvent, m_dwTimeout) == WAIT_TIMEOUT)
	{
		// Do some work
		SYSTEMTIME SystemTime;
		GetLocalTime(&SystemTime);

		fout<< SystemTime.wYear << '-' << SystemTime.wMonth << '-' << SystemTime.wDay << '\t'
			<< SystemTime.wHour << ':' << SystemTime.wMinute << ':' << SystemTime.wDay << std::endl;
	}

	// Signal the stopped event.
	SetEvent(m_hStoppedEvent);
    WriteErrorLogEntry(L"CSampleService::ServiceWorkerThread: done");
}


//
//   FUNCTION: CSampleService::OnStop(void)
//
//   PURPOSE: The function is executed when a Stop command is sent to the 
//   service by SCM. It specifies actions to take when a service stops 
//   running. In this code, OnStop logs a service-stop message to the 
//   Application log, and waits for the finish of the main service function.
//
void CSampleService::OnStop()
{
	SetServiceStatus(SERVICE_STOP_PENDING, ERROR_SUCCESS, 30 * 1000);

    WriteErrorLogEntry(L"CSampleService::Stop: function entry");

	// Log a service stop message to the Application log.
	WriteEventLogEntry(L"CppWindowsService in OnStop", EVENTLOG_INFORMATION_TYPE);

	// Indicate that the service is stopping and wait for the finish of the 
	// main service function (ServiceWorkerThread).
	SetEvent(m_hStoppingEvent);
	if (WaitForSingleObject(m_hStoppedEvent, INFINITE) != WAIT_OBJECT_0)
	{
		SetServiceStatus(SERVICE_STOP_PENDING, ERROR_INVALID_DATA, 30 * 1000);
	    WriteErrorLogEntry(L"OnStop: Service Start", GetLastError());
		throw GetLastError();
	}

    WriteErrorLogEntry(L"CSampleService::Stop: function exit");
}
