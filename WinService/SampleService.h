/*******************************************************************************\
* Provides a sample service class that derives from the service base class - 
* CServiceBase. The sample service logs the service start and stop 
* information to the Application event log, and shows how to run the main 
* function of the service in a thread pool worker thread.

\***************************************************************************/

#pragma once

#include "ServiceBase.h"


class CSampleService : public CServiceBase
{
public:
    CSampleService(PWSTR pszServiceName, 
        BOOL fCanStop = TRUE, 
        BOOL fCanShutdown = TRUE, 
        BOOL fCanPauseContinue = FALSE);
    virtual ~CSampleService(void);

	HANDLE GetStopEvent() { return m_hStoppedEvent; }

protected:
    virtual void OnStart(DWORD dwArgc, PWSTR *pszArgv);
    virtual void OnStop();

    void ServiceWorkerThread(void);

private:
	DWORD m_dwTimeout;
    HANDLE m_hStoppingEvent;	// just used internally
    HANDLE m_hStoppedEvent;
};